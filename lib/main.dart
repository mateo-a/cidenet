import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

DateTime currentTime = DateTime.now();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _lastName1 = TextEditingController();
  final _lastName2 = TextEditingController();
  final _name = TextEditingController();
  final _otherName = TextEditingController();
  final _email = TextEditingController();

  final now = new DateFormat('dd.MM.yyyy HH:mm:ss').format(DateTime.now());

// Text(f.format(new DateTime.fromMillisecondsSinceEpoch(values[index]["start_time"]*1000)));

  var _listCountry = ["Colombia", "Estados Unidos"];
  String _country = "Seleccione...";

  var _listTypeId = [
    "Cédula de Ciudadanía",
    "Cédula de Extranjería",
    "Pasaporte",
    "Permiso Especial"
  ];
  String _typeId = "Seleccione...";

  var _listArea = [
    "Administración",
    "Financiera",
    "Compras",
    "Infraestructura",
    "Operación",
    "Talento Humano",
    "Servicios Varios"
  ];
  String _area = "Seleccione...";
  String _emailGenerate = "";

  String validateDomine(String country) {
    if (_country == 'Colombia') {
      return "cidenet.com.co";
    } else if (_country == 'Estados Unidos') {
      return "cidenet.com.us";
    }
    return null;
  }

  /* validateUniqueEmail: Function to check if the email generated is unique
  *  _emailFull: String received conaining the email generated
  * Return: True if the email is unique otherwise false
  */
  // bool validateUniqueEmail(_emailFull) {
  //   if (_emailFull == 'alex.urrego.5@cidenet.com.co') {
  //     return true;
  //   }
  //   return false;
  // }

  void generateEmail() {
    String _domine = validateDomine(_country);
    String _emailName = _name.text.toLowerCase().replaceAll(' ', '');
    String _emailLastName = _lastName1.text.toLowerCase().replaceAll(' ', '');
    // int id = 0;  /* Variable to set the ID value in case the email is not unique */
    String _emailFull = "$_emailName.$_emailLastName@$_domine";

    /*
    * Loop to invoque a validateUniqueEmail function in order to check if the mail generated is unique
    */
    // while (!validateUniqueEmail(_emailFull)) {
    //   id += 1;
    //   _emailFull = "$_emailName.$_emailLastName.$id@$_domine";
    //   validateUniqueEmail(_emailFull);
    //   print(_emailFull);
    // }

    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);

    if (_emailFull.length > 300) {
      setState(() {
        _emailGenerate = "Error, Email supera 300 caracteres";
      });
    } else if (regExp.hasMatch(_emailFull)) {
      setState(() {
        _emailGenerate = _emailFull;
      });
    } else {
      setState(() {
        _emailGenerate = "Falta información necesaria para crear email";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("Registro de Empleados"),
        ),
        body: Form(
          key: _key,
          child: SingleChildScrollView(
            // reverse: true,
            padding: EdgeInsets.all(25),
            child: Column(
              children: [
                TextFormField(
                  controller: _lastName1,
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(20),
                    FilteringTextInputFormatter(RegExp("[A-Z ]"), allow: true),
                    FilteringTextInputFormatter(RegExp("^[ ]"), allow: false),
                  ],
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    labelText: 'Primer Apellido',
                  ),
                  textCapitalization: TextCapitalization.characters,
                  validator: validateText,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                TextFormField(
                  controller: _lastName2,
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(20),
                    FilteringTextInputFormatter(RegExp("[A-Z ]"), allow: true),
                    FilteringTextInputFormatter(RegExp("^[ ]"), allow: false),
                  ],
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    labelText: 'Segundo Apellido',
                  ),
                  textCapitalization: TextCapitalization.characters,
                  validator: validateText,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                TextFormField(
                  controller: _name,
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(20),
                    FilteringTextInputFormatter(RegExp("[A-Z ]"), allow: true),
                    FilteringTextInputFormatter(RegExp("^[ ]"), allow: false),
                  ],
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    labelText: 'Primer Nombre',
                  ),
                  textCapitalization: TextCapitalization.characters,
                  validator: validateText,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                TextFormField(
                  controller: _otherName,
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(50),
                    FilteringTextInputFormatter(RegExp("[A-Z ]"), allow: true),
                    FilteringTextInputFormatter(RegExp("^[ ]"), allow: false),
                  ],
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    labelText: 'Otros Nombres',
                  ),
                  textCapitalization: TextCapitalization.characters,
                  validator: validateOtherText,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 5.0),
                  ),
                  Text("País del Empleo:"),
                  Padding(
                    padding: EdgeInsets.only(right: 45.0),
                  ),
                  DropdownButton<String>(
                      items: _listCountry.map((String val) {
                        return DropdownMenuItem<String>(
                            value: val, child: Text(val));
                      }).toList(),
                      onChanged: (String _value) => {
                            setState(() {
                              _country = _value;
                            })
                          },
                      hint: Text(_country)),
                ]),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 5.0),
                  ),
                  Text("Tipo de Identificación:"),
                  Padding(
                    padding: EdgeInsets.only(left: 10.0),
                  ),
                  DropdownButton<String>(
                      items: _listTypeId.map((String val) {
                        return DropdownMenuItem<String>(
                            value: val, child: Text(val));
                      }).toList(),
                      onChanged: (String _value) => {
                            setState(() {
                              _typeId = _value;
                            })
                          },
                      hint: Text(_typeId)),
                ]),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                TextFormField(
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(20),
                    FilteringTextInputFormatter(RegExp("[A-Za-z0-9-]"),
                        allow: true),
                    FilteringTextInputFormatter(RegExp("^[ ]"), allow: false),
                  ],
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    labelText: 'Número de Identificación',
                  ),
                  validator: validateId,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                TextFormField(
                  controller: _email,
                  enabled: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    labelText: _emailGenerate,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    labelText: 'Fecha de ingreso',
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 5.0),
                  ),
                  Text("Área:"),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0),
                  ),
                  DropdownButton<String>(
                      items: _listArea.map((String val) {
                        return DropdownMenuItem<String>(
                            value: val, child: Text(val));
                      }).toList(),
                      onChanged: (String _value) => {
                            setState(() {
                              _area = _value;
                            })
                          },
                      hint: Text(_area)),
                ]),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 5.0),
                    ),
                    Text("Estado:"),
                    Padding(
                      padding: EdgeInsets.only(left: 10.0),
                    ),
                    Text("Activo"),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 5.0),
                    ),
                    Text("Fecha y hora de registro:"),
                    Padding(
                      padding: EdgeInsets.only(left: 10.0),
                    ),
                    Text(now),
                  ],
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.save),
          onPressed: () {
            if (_key.currentState.validate()) {
              print("Formulario enviado correctamente");
            }
            generateEmail();
          },
        ),
      ),
    );
  }
}

String validateText(String formText) {
  if (formText.isEmpty) return 'Información requerida.';
  String checkSpace = r'\s{1,}';
  String _singleSpace = formText.replaceAll(RegExp(checkSpace), ' ').trim();

  if (_singleSpace.length > 20) return 'Debe ingresar menos de 20 letras';

  return null;
}

String validateOtherText(String formText) {
  String check = r'\s{1,}';
  String _singleSpace = formText.replaceAll(RegExp(check), ' ').trim();

  if (_singleSpace.length > 50) return 'Debe ingresar menos de 50 letras';

  return null;
}

String validateId(String formText) {
  if (formText.length > 20) return 'Debe ingresar menos de 20 caracteres';

  return null;
}
